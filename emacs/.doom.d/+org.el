;;; ~/dotfiles/emacs/.doom.d/+org.el -*- lexical-binding: t; -*-


(add-hook! 'org-mode-hook #'mixed-pitch-mode)

(setq
 org-use-property-inheritance t              ; it's convenient to have properties inherited
 org-log-done 'time                          ; having the time a item is done sounds convininet
 org-list-allow-alphabetical t               ; have a. A. a) A) list bullets
 ;; org-export-in-background t                  ; run export processes in external emacs process
 org-catch-invisible-edits 'smart            ; try not to accidently do weird stuff in invisible regions
 )

(defvar -org-default-projects-dir (concat org-directory "" )
  "Primary GTD directory.")

(defvar -org-default-inbox-file
  (concat -org-default-projects-dir "inbox.org")
  "New stuff collects in this file.")

(defvar -org-default-tasks-file
  (concat -org-default-projects-dir "next.org")
  "Tasks, TODOs and little projects.")

(defvar -org-default-incubate-file
  (concat -org-default-projects-dir "incubate.org")
  "Ideas simmering on backburner.")

(defvar -org-default-projects-file
  (concat -org-default-projects-dir "projects.org")
  "A list of current projects")

(defvar -org-default-tickler-file
  (concat -org-default-projects-dir "tickler.org")
  "A list of appointments and deadlines")

(defvar -org-default-completed-file
  nil
  "Finished projects")

(defvar -org-default-media-file
  (concat -org-default-projects-dir "media.org")
  "White papers and links to other things to check out.")


(defvar -org-capture-recipes
  (concat -org-default-projects-dir "recipes.org")
  "White papers and links to other things to check out.")


;; play youtube videos in MPV
(defun my/online-to-video-player (link)
  "Open open url of the video with url"
  (interactive)
  (let* ((video-player-command "celluloid") ;; set video-player-command to mpv
         (online-video-url link)) ;; retrieves url
    ;; Run as sub-process.
    (start-process-shell-command
     (concat video-player-command " " online-video-url)
     nil
     (concat video-player-command " " online-video-url))
    (message "Opening online video with %s !" video-player-command)))


;; -----------Org mode config
(after! org
  (require 'ox-extra)
  (ox-extras-activate '(ignore-headlines))

  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-show-habits-only-for-today nil)

  (setq +org-capture-todo-file "projects/inbox.org")
  (setq +org-capture-projects-file "projects/projects.org")
  ;; Add type of link to open youtube videos in celluloid
  (org-link-set-parameters "yt-MPV" :follow #'my/online-to-video-player)

  ;; adds refile targets
  (setq org-refile-targets '((-org-default-tasks-file :maxlevel . 2)
                             (-org-default-incubate-file :level . 2)
                             (-org-default-tickler-file :maxlevel . 2)
                             (-org-default-projects-file :maxlevel . 3)
                             (-org-default-media-file :maxlevel . 2)
                             ("~/my_files/projects/goals.org" :maxlevel . 2)
                             ("~/my_files/projects/master.org" :maxlevel . 4)
                             ("~/my_files/projects/work.org" :maxlevel . 4)
                             (nil . (:maxlevel . 4))
                                  ))


        (setq org-latex-pdf-process (list "latexmk -f -pdf -%latex -shell-escape -interaction=nonstopmode -output-directory=%o %f"))
        (setq org-latex-listings 'minted
              org-latex-packages-alist '(("" "minted")))

        (setq org-latex-hyperref-template nil)
        (setq org-export-allow-bind-keywords t)
        (setq org-export-with-title nil)
        (setq org-latex-title-command nil)
        (setq org-export-with-author nil)
        (setq org-export-with-date nil)
        (setq org-export-headline-levels 5) ; I like nesting

        (defun org-current-is-todo ()
          (string= "TODO" (org-get-todo-state)))

        (defun org-archive-done (&optional arg)
          (when( org-current-is-todo)
            (org-todo 'done)))

        (advice-add 'org-archive-subtree :before 'org-archive-done)

        (defun todays-journal-entry ()
          "Return the full pathname to the day's journal entry file.
    Granted, this assumes each journal's file entry to be formatted
    with year/month/day, as in `20190104' for January 4th.

    Note: `org-journal-dir' variable must be set to the directory
    where all good journal entries live, e.g. ~/journal."
          (let* ((daily-name   (format-time-string "%Y%m%d"))
                 (file-name    (concat org-journal-dir daily-name)))
            (expand-file-name file-name)))

        (setq org-todo-keywords
              '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)")))

        (setq org-log-done 'time
              org-log-into-drawer t
              org-log-state-notes-insert-after-drawers nil)

        (setq org-tag-alist (quote (("@errand" . ?e)
                                    ("@office" . ?o)
                                    ("@home" . ?h)
                                    ("@school" . ?s)
                                    ("uni" . ?u)
                                    ("@work" . ?w)
                                    (:newline)
                                    ("WAITING" . ?W)
                                    ("HOLD" . ?H)
                                    ("CANCELLED" . ?C))))

        (setq org-fast-tag-selection-single-key nil)
        (setq org-refile-use-outline-path 'file
              org-outline-path-complete-in-steps nil)
        (setq org-refile-allow-creating-parent-nodes 'confirm)

        ;; (add-to-list 'org-agenda-custom-commands '(my/org-agenda-todo-view))

        (add-to-list 'org-capture-templates
                     '("i" "inbox" entry (file -org-default-inbox-file)
                       "* TODO %?" :empty-lines 1))

        (add-to-list 'org-capture-templates
                     '("n" "random note" entry (file -org-default-inbox-file)
                       "* %? \n %u" :empty-lines 1))

        (add-to-list 'org-capture-templates
                     '("T" "Tickler" entry (file -org-default-tickler-file)
                       "* %i%? %^G \n %^t"))

        (add-to-list 'org-capture-templates
                     '("c" "Cookbook" entry (file "~/my_files/cookbook.org")
                        "%(org-chef-get-recipe-from-url)"
                        :empty-lines 1))

        (add-to-list 'org-capture-templates
                     '("m" "Manual Cookbook" entry (file "~/my_files/cookbook.org")
                       "* %^{Recipe title: }\n  :PROPERTIES:\n  :source-url:\n  :servings:\n  :prep-time:\n  :cook-time:\n  :ready-in:\n  :END:\n** Ingredients\n   %?\n** Directions\n\n"))


        (defun org-journal-find-location ()
          ;; Open today's journal, but specify a non-nil prefix argument in order to
          ;; inhibit inserting the heading; org-capture will insert the heading.
          (org-journal-new-entry t)
          (org-narrow-to-subtree)
          (goto-char (point-max)))

        (add-to-list 'org-capture-templates
                     '("d" "diary"))

        (add-to-list 'org-capture-templates
                     '("dd" "Distraction" plain (function org-journal-find-location)
                       "** %(format-time-string org-journal-time-format) Distraction \t:distraction:\n%i%?"
                       ))

        (add-to-list 'org-capture-templates
                     '("ds" "New Diary Summary" entry (file+olp+datetree "~/my_files/reviews.org")
                       (file "~/my_files/projects/templates/diary_summary.org")))
        ;; (add-to-list 'org-capture-templates
        ;;              '("dl" "New Diary log" entry(function org-journal-find-location)
        ;;                (file "~/my_files/projects/templates/diary_log.org")))

        ;; (add-to-list 'org-capture-templates
        ;;              '("a" "Activities" entry (file+olp+datetree "~/my_files/projects/activities.org")
        ;;                "* %?\n  %i\n"))
        )

  (defun my-new-weekly-review ()
    (interactive)
    (let ((org-capture-templates '(("w" "Review: Weekly Review" entry (file+olp+datetree "~/my_files/reviews.org")
                                    (file "~/my_files/projects/templates/weekly_review.org")))))
      (progn
        (org-capture nil "w")
        (org-capture-finalize t)
        (org-speed-move-safe 'outline-up-heading)
        (org-narrow-to-subtree)
        ;; (fetch-calendar)
        (org-clock-in)
        )))


  (after! org-journal
    (setq
     org-journal-file-header 'org-journal-file-header-func
     ;; org-journal-enable-agenda-integration t
     org-journal-file-type `daily
     org-journal-encrypt-journal t
     )

    (defun org-journal-file-header-func (time)
      "Custom function to create journal header."
      (concat
       (pcase org-journal-file-type
         (`daily "#+TITLE: Daily Journal\n#+STARTUP: showeverything")
         (`weekly "#+TITLE: Weekly Journal\n#+STARTUP: folded")
         (`monthly "#+TITLE: Monthly Journal\n#+STARTUP: folded")
         (`yearly "#+TITLE: Yearly Journal\n#+STARTUP: folded"))))
    )

  (after! org-roam
    (setq org-id-link-to-org-use-id t)
    (setq org-roam-capture-templates
          '(("d" "default" plain
             "%?"
             :target (file+head "inbox/${slug}.org"
                                "#+title: ${title}\n#+date: %U\n")
             :immediate-finish t
             :unnarrowed t)

            ("r" "bibliography reference" plain "%?"
             :target
             (file+head "references/${citekey}.org"
                        ":PROPERTIES:
:ROAM_REFS: @${citekey}
:END:
#+title: ${author-abbrev}(${date})-${title}\n#+date: %U\n")
             :unnarrowed t)))
    (setq org-roam-capture-ref-templates
          '(("r" "ref" plain "%?"
             :target (file+head "bookmarks/${slug}.org"
                                "#+title: ${title}\n#+date: %U\n")
             :unnarrowed t))))

  (use-package! websocket
    :after org-roam)

  (use-package! org-roam-ui
    :after org-roam
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

  (after! org-noter
    (setq org-noter-notes-search-path "~/references/notes/"))

  (setq org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)

  ;; use this webpage
  ;; https://blog.phundrak.com/better-custom-ids-orgmode/

  (defun cp/org-id-new (&optional prefix)
    "Create a new globally unique ID.

An ID consists of two parts separated by a colon:
- a prefix
- a   unique   part   that   will   be   created   according   to
  `org-id-method'.

PREFIX  can specify  the  prefix,  the default  is  given by  the
variable  `org-id-prefix'.  However,  if  PREFIX  is  the  symbol
`none', don't  use any  prefix even if  `org-id-prefix' specifies
one.

So a typical ID could look like \"Org-4nd91V40HI\"."
    (let* ((prefix (if (eq prefix 'none)
                       ""
                     (concat (or prefix org-id-prefix)
                             "-"))) unique)
      (if (equal prefix "-")
          (setq prefix ""))
      (cond
       ((memq org-id-method
              '(uuidgen uuid))
        (setq unique (org-trim (shell-command-to-string org-id-uuid-program)))
        (unless (org-uuidgen-p unique)
          (setq unique (org-id-uuid))))
       ((eq org-id-method 'org)
        (let* ((etime (org-reverse-string (org-id-time-to-b36)))
               (postfix (if org-id-include-domain
                            (progn
                              (require 'message)
                              (concat "@"
                                      (message-make-fqdn))))))
          (setq unique (concat etime postfix))))
       (t (error "Invalid `org-id-method'")))
      (concat prefix (car (split-string unique "-")))))


  (defun cp/org-custom-id-get (&optional pom create prefix)
    "Get the CUSTOM_ID property of the entry at point-or-marker POM.

If POM is nil, refer to the entry at point. If the entry does not
have an CUSTOM_ID, the function returns nil. However, when CREATE
is non nil, create a CUSTOM_ID if none is present already. PREFIX
will  be passed  through to  `cp/org-id-new'. In  any case,  the
CUSTOM_ID of the entry is returned."
    (interactive)
    (org-with-point-at pom
      (let* ((orgpath (mapconcat #'identity (org-get-outline-path) "-"))
             (heading (replace-regexp-in-string
                       "/\\|~\\|\\[\\|\\]" ""
                       (replace-regexp-in-string
                        "[[:space:]]+" "_" (if (string= orgpath "")
                                               (org-get-heading t t t t)
                                             (concat orgpath "-" (org-get-heading t t t t))))))
             (id (org-entry-get nil "CUSTOM_ID")))
        (cond
         ((and id
               (stringp id)
               (string-match "\\S-" id)) id)
         (create (setq id (cp/org-id-new (concat prefix heading)))
                 (org-entry-put pom "CUSTOM_ID" id)
                 (org-id-add-location id
                                      (buffer-file-name (buffer-base-buffer)))
                 id)))))

  (defun cp/org-add-ids-to-headlines-in-file ()
    "Add CUSTOM_ID properties to all headlines in the current
   file which do not already have one. Only adds ids if the
   `auto-id' option is set to `t' in the file somewhere. ie,
   #+OPTIONS: auto-id:t"
    (interactive)
    (save-excursion
      (widen)
      (goto-char (point-min))
      (when (re-search-forward "^#\\+OPTIONS:.*auto-id:t" (point-max) t)
        (org-map-entries (lambda () (cp/org-custom-id-get (point) 'create))))))

  (require 's)
  (defun cp/auto-custom-id ()
    (interactive)
    (let* ((heading (org-entry-get nil "ITEM"))
           (custom-id-auto (s-dashed-words (s-downcase (car (s-split "\\[" (car (s-split "(" heading))))))))
      (org-entry-put nil "CUSTOM_ID" custom-id-auto)))


  (add-hook 'org-mode-hook
            (lambda ()
              (add-hook 'after-save-hook #'cp/org-add-ids-to-headlines-in-file nil
                        'make-it-local)))
  ;; (add-hook 'org-mode-hook #'auto-fill-mode)

  (after! org-pomodoro
    ;; reduce pomodoro volume
    (setq org-pomodoro-finished-sound-args    "--volume=50000")
    (setq org-pomodoro-short-break-sound-args "--volume=50000")
    (setq org-pomodoro-killed-sound-args      "--volume=50000")
    (setq org-pomodoro-ticking-sound-args     "--volume=50000")
    (setq org-pomodoro-start-sound-args       "--volume=50000")
    (setq org-pomodoro-long-break-sound-args  "--volume=50000")
    (setq org-pomodoro-overtime-sound-args    "--volume=50000"))

  (load! "+agenda.el")
  ;; (autoload 'gac-after-save-func "git-auto-commit-mode")
  ;; (add-hook! 'org-mode-hook
  ;;   (add-hook 'kill-buffer-hook #'gac-after-save-func t t))
  ;; (use-package! ox-awesomecv
  ;;   :after org)
  ;; (use-package! ox-awesomecv2
  ;;   :after org)

  (custom-set-faces!
    '(outline-1 :weight extra-bold :height 1.25)
    '(outline-2 :weight bold :height 1.15)
    '(outline-3 :weight bold :height 1.12)
    '(outline-4 :weight semi-bold :height 1.09)
    '(outline-5 :weight semi-bold :height 1.06)
    '(outline-6 :weight semi-bold :height 1.03)
    '(outline-8 :weight semi-bold)
    '(outline-9 :weight semi-bold))

  (after! org-superstar
    (setq org-superstar-headline-bullets-list '("◉" "○" "✸" "✿" "✤" "✜" "◆" "▶")
          ;; org-superstar-headline-bullets-list '("Ⅰ" "Ⅱ" "Ⅲ" "Ⅳ" "Ⅴ" "Ⅵ" "Ⅶ" "Ⅷ" "Ⅸ" "Ⅹ")
          org-superstar-prettify-item-bullets t ))
  (after! org
    (custom-set-faces!
      '(org-document-title :height 1.2))
    (setq
     ;; org-ellipsis " ▾ "
     org-hide-leading-stars t
     org-priority-highest ?A
     org-priority-lowest ?E
     org-priority-faces
     '((?A . 'all-the-icons-red)
       (?B . 'all-the-icons-orange)
       (?C . 'all-the-icons-yellow)
       (?D . 'all-the-icons-green)
       (?E . 'all-the-icons-blue))))

(after! org
  (add-to-list 'org-export-filter-headline-functions
               (defun my-latex-filter-removeOrgAutoLabels (text backend info)
                 "Org-mode automatically generates labels for headings despite explicit use of `#+LABEL`. This filter forcibly removes all automatically generated org-labels in headings."
                 (when (org-export-derived-backend-p backend 'latex)
                   (replace-regexp-in-string "\\\\label{sec:org[a-f0-9]+}\n" "" text))
                 )))

(use-package! org-chef
  :commands (org-chef-insert-recipe org-chef-get-recipe-from-url))
