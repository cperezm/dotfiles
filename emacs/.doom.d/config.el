;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!


;; These are used for a number of things, particularly for GPG configuration,
;; some email clients, file templates and snippets.
(setq user-full-name "Carlos Perez"
      user-mail-address "carlosperezmolano@gmail.com")

;; start full screen
(add-hook 'window-setup-hook #'toggle-frame-fullscreen)

;; transparency
;; (set-frame-parameter (selected-frame) 'alpha '(97 . 97))
;; (add-to-list 'default-frame-alist '(alpha . (97 . 97)))

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "Fira Code" :size 16)
;;       doom-variable-pitch-font (font-spec :family "Overpass" :size 18))

(setq doom-font (font-spec :family "JetBrains Mono" :size 21)
      doom-variable-pitch-font (font-spec :family "Overpass Nerd Font" :size 25)
      doom-unicode-font (font-spec :family "JuliaMono")
      doom-serif-font (font-spec :family "IBM Plex Mono" :weight 'light)
      )

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
(setq doom-theme 'doom-flatwhite)
;; (setq doom-theme 'doom-solarized-light)
;; (setq doom-theme 'leuven)

;; If you intend to use org, it is recommended you change this!
;; Directory variables
(setq org-directory "~/my_files/projects/")
(setq org-roam-directory (file-truename "~/braindump/org/"))
(setq org-roam-db-location "~/org-roam.db")
(setq org-journal-dir "~/my_files/journal/")
(setq! citar-bibliography (append
                           (file-expand-wildcards "~/references/master_thesis/*")
                           (file-expand-wildcards "~/masterHMDA/thesis/master_thesis_document/mad.bib")
                           `("~/references/citations.bib")
                           `("~/references/master_thesis.bib")
                           `("~/masterHMDA/thesis/master_thesis_document/mad.bib")
                           ))

(setq! citar-library-paths '("~/references")
       citar-notes-paths '("~/braindump/org/references"))

(setq citar-templates
      '((main . "${author editor:30}     ${date year issued:4}     ${title:48}")
        (suffix . "          ${=key= id:15}    ${=type=:12}    ${tags keywords:*}")
        (preview . "${author editor} (${year issued date}) ${title}, ${journal journaltitle publisher container-title collection-title}.\n")
        (note . "#+title: ${author-abbrev}(${date})-${title}")))

(setq! citar-latex-prompt-for-cite-style 'nil)

(setq org-cite-csl-styles-dir "~/Zotero/styles")

(load! "+org.el")

(setq ispell-personal-dictionary (expand-file-name ".ispell_personal" doom-private-dir))
(setq ispell-dictionary "en")

;; If you want to change the style of line numbers, change this to `relative' or
;; `nil' to disable it:
(setq display-line-numbers-type `relative)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

;; general variables
(setq-default
 delete-by-moving-to-trash t                      ; Delete files to trash
 window-combination-resize t                      ; take new window space from all other windows (not just current)
 x-stretch-cursor t)                              ; Stretch cursor to the glyph width

(setq undo-limit 80000000                         ; Raise undo-limit to 80Mb
      evil-want-fine-undo t                       ; By default while in insert all changes are one big blob. Be more granular
      password-cache-expiry nil                   ; I can trust my computers ... can't I?
      scroll-margin 1)                            ; It's nice to maintain a little margin

(display-time-mode 1)                             ; Enable time in the mode-line

(unless (string-match-p "^Power N/A" (battery))   ; On laptops...
  (display-battery-mode 1))                       ; it's nice to know how much power you have

(global-subword-mode 1)                           ; Iterate through CamelCase words


(remove-hook 'text-mode-hook #'visual-line-mode)

(add-hook 'text-mode-hook #'auto-fill-mode)


;; (add-hook 'writeroom-mode-hook #'turn-off-flyspell)
(add-hook! 'writeroom-mode-hook
           ;; (flyspell-mode (if writeroom-mode -1 +1))
           (spell-fu-mode (if writeroom-mode -1 +1))
           (display-line-numbers-mode (if writeroom-mode -1 +1))
           (writegood-mode (if writeroom-mode -1 +1))
           )

;; (after! forge
;;   (add-to-list 'forge-alist
;;                '("mad-srv.informatik.uni-erlangen.de"
;;                  "mad-srv.informatik.uni-erlangen.de/api/v4"
;;                  "mad-srv.informatik.uni-erlangen.de"
;;                  forge-gitlab-repository))
;;   (add-to-list 'forge-alist
;;                '("gitlab.cs.fau.de"
;;                  "gitlab.cs.fau.de/api/v4"
;;                  "gitlab.cs.fau.de"
;;                  forge-gitlab-repository)))

(add-hook! 'magit-process-find-password-functions
           #'magit-process-password-auth-source)

(after! magit
  (magit-wip-mode)

  (defun th/magit--with-difftastic (buffer command)
    "Run COMMAND with GIT_EXTERNAL_DIFF=difft then show result in BUFFER."
    (let ((process-environment
           (cons (concat "GIT_EXTERNAL_DIFF=difft --width="
                         (number-to-string (frame-width)))
                 process-environment)))
      ;; Clear the result buffer (we might regenerate a diff, e.g., for
      ;; the current changes in our working directory).
      (with-current-buffer buffer
        (setq buffer-read-only nil)
        (erase-buffer))
      ;; Now spawn a process calling the git COMMAND.
      (make-process
       :name (buffer-name buffer)
       :buffer buffer
       :command command
       ;; Don't query for running processes when emacs is quit.
       :noquery t
       ;; Show the result buffer once the process has finished.
       :sentinel (lambda (proc event)
                   (when (eq (process-status proc) 'exit)
                     (with-current-buffer (process-buffer proc)
                       (goto-char (point-min))
                       (ansi-color-apply-on-region (point-min) (point-max))
                       (setq buffer-read-only t)
                       (view-mode)
                       (end-of-line)
                       ;; difftastic diffs are usually 2-column side-by-side,
                       ;; so ensure our window is wide enough.
                       (let ((width (current-column)))
                         (while (zerop (forward-line 1))
                           (end-of-line)
                           (setq width (max (current-column) width)))
                         ;; Add column size of fringes
                         (setq width (+ width
                                        (fringe-columns 'left)
                                        (fringe-columns 'right)))
                         (goto-char (point-min))
                         (pop-to-buffer
                          (current-buffer)
                          `(;; If the buffer is that wide that splitting the frame in
                            ;; two side-by-side windows would result in less than
                            ;; 80 columns left, ensure it's shown at the bottom.
                            ,(when (> 80 (- (frame-width) width))
                               #'display-buffer-at-bottom)
                            (window-width
                             . ,(min width (frame-width))))))))))))

  (defun th/magit-show-with-difftastic (rev)
    "Show the result of \"git show REV\" with GIT_EXTERNAL_DIFF=difft."
    (interactive
     (list (or
            ;; If REV is given, just use it.
            (when (boundp 'rev) rev)
            ;; If not invoked with prefix arg, try to guess the REV from
            ;; point's position.
            (and (not current-prefix-arg)
                 (or (magit-thing-at-point 'git-revision t)
                     (magit-branch-or-commit-at-point)))
            ;; Otherwise, query the user.
            (magit-read-branch-or-commit "Revision"))))
    (if (not rev)
        (error "No revision specified")
      (th/magit--with-difftastic
       (get-buffer-create (concat "*git show difftastic " rev "*"))
       (list "git" "--no-pager" "show" "--ext-diff" rev))))

  (defun th/magit-diff-with-difftastic (arg)
    "Show the result of \"git diff ARG\" with GIT_EXTERNAL_DIFF=difft."
    (interactive
     (list (or
            ;; If RANGE is given, just use it.
            (when (boundp 'range) range)
            ;; If prefix arg is given, query the user.
            (and current-prefix-arg
                 (magit-diff-read-range-or-commit "Range"))
            ;; Otherwise, auto-guess based on position of point, e.g., based on
            ;; if we are in the Staged or Unstaged section.
            (pcase (magit-diff--dwim)
              ('unmerged (error "unmerged is not yet implemented"))
              ('unstaged nil)
              ('staged "--cached")
              (`(stash . ,value) (error "stash is not yet implemented"))
              (`(commit . ,value) (format "%s^..%s" value value))
              ((and range (pred stringp)) range)
              (_ (magit-diff-read-range-or-commit "Range/Commit"))))))
    (let ((name (concat "*git diff difftastic"
                        (if arg (concat " " arg) "")
                        "*")))
      (th/magit--with-difftastic
       (get-buffer-create name)
       `("git" "--no-pager" "diff" "--ext-diff" ,@(when arg (list arg))))))

  (transient-define-prefix th/magit-aux-commands ()
    "My personal auxiliary magit commands."
    ["Auxiliary commands"
     ("d" "Difftastic Diff (dwim)" th/magit-diff-with-difftastic)
     ("s" "Difftastic Show" th/magit-show-with-difftastic)])

  (transient-append-suffix 'magit-dispatch "!"
    '("#" "My Magit Cmds" th/magit-aux-commands))

  (define-key magit-status-mode-map (kbd "#") #'th/magit-aux-commands)

  )

(after! latex
  ;; File types
  (add-to-list 'auto-mode-alist '("\\.sty\\'"  . LaTeX-mode))

  (setq TeX-engine-alist '((default
                             "Tectonic"
                             "tectonic -X compile -f plain %T"
                             "tectonic -X watch"
                             nil)))

  (setq LaTeX-command-style '(("" "%(latex)")))

  (setq TeX-process-asynchronous t
        TeX-check-TeX nil
        TeX-engine 'default)

  (let ((tex-list (assoc "TeX" TeX-command-list))
        (latex-list (assoc "LaTeX" TeX-command-list)))
    (setf (cadr tex-list) "%(tex)"
          (cadr latex-list) "%l"))

  (add-hook 'after-change-major-mode-hook
            (lambda ()
              (when-let ((project (project-current))
                         (proot (project-root project)))
                (when (file-exists-p (expand-file-name "Tectonic.toml" proot))
                  (setq-local TeX-output-dir (expand-file-name "build/default" proot))))))
  )

(setq magit-repository-directories `(("/home/cperezm/" . 1)))

;; (add-hook 'org-mode-hook
;;           (lambda ()
;;             (setq spell-fu-faces-exclude '(org-meta-line org-link org-code))
;;             (spell-fu-mode)))

;; Grammar check
(setq langtool-disabled-rules '("WHITESPACE_RULE"
                                "EN_UNPAIRED_BRACKETS"
                                "COMMA_PARENTHESIS_WHITESPACE"
                                "EN_QUOTES"
                                "MORFOLOGIK_RULE_EN_GB"
                                "MORFOLOGIK_RULE_EN_US"
                                "UNLIKELY_OPENING_PUNCTUATION"
                                "PLUS_MINUS"
                                ))

;; (package! spell-fu :disable t)

;; (after! spell-fu
;;   (add-to-list 'ispell-skip-region-alist '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:"))
;;   (add-to-list 'ispell-skip-region-alist '("#\\+BEGIN_SRC" . "#\\+END_SRC")
;;                ))




;; reduce zoom in writeroom mode
(setq +zen-text-scale 0.8)


(use-package! ledger-mode
  :mode ("\\.journal\\'" . ledger-mode)
  )


;; (defvar mixed-pitch-modes '(org-mode LaTeX-mode markdown-mode gfm-mode Info-mode)
;;   "Modes that `mixed-pitch-mode' should be enabled in, but only after UI initialisation.")

;; (defun init-mixed-pitch-h ()
;;   "Hook `mixed-pitch-mode' into each mode in `mixed-pitch-modes'.
;; Also immediately enables `mixed-pitch-modes' if currently in one of the modes."
;;   (when (memq major-mode mixed-pitch-modes)
;;     (mixed-pitch-mode 1))
;;   (dolist (hook mixed-pitch-modes)
;;     (add-hook (intern (concat (symbol-name hook) "-hook")) #'mixed-pitch-mode)))
;; (add-hook 'doom-init-ui-hook #'init-mixed-pitch-h)


;; (autoload #'mixed-pitch-serif-mode "mixed-pitch"
;;   "Change the default face of the current buffer to a serifed variable pitch, while keeping some faces fixed pitch." t)

;; (after! mixed-pitch
;;   (defface variable-pitch-serif
;;     '((t (:family "serif")))
;;     "A variable-pitch face with serifs."
;;     :group 'basic-faces)
;;   (setq mixed-pitch-set-height t)
;;   (setq variable-pitch-serif-font (font-spec :family "Alegreya" :size 24))
;;   (set-face-attribute 'variable-pitch-serif nil :font variable-pitch-serif-font)
;;   (defun mixed-pitch-serif-mode (&optional arg)
;;     "Change the default face of the current buffer to a serifed variable pitch, while keeping some faces fixed pitch."
;;     (interactive)
;;     (let ((mixed-pitch-face 'variable-pitch-serif))
;;       (mixed-pitch-mode (or arg 'toggle)))))

;; (defvar +zen-serif-p t
;;   "Whether to use a serifed font with `mixed-pitch-mode'.")
;; (after! writeroom-mode
;;   (defvar-local +zen--original-org-indent-mode-p nil)
;;   (defvar-local +zen--original-mixed-pitch-mode-p nil)
;;   (defvar-local +zen--original-org-pretty-table-mode-p nil)
;;   (defun +zen-enable-mixed-pitch-mode-h ()
;;     "Enable `mixed-pitch-mode' when in `+zen-mixed-pitch-modes'."
;;     (when (apply #'derived-mode-p +zen-mixed-pitch-modes)
;;       (if writeroom-mode
;;           (progn
;;             (setq +zen--original-mixed-pitch-mode-p mixed-pitch-mode)
;;             (funcall (if +zen-serif-p #'mixed-pitch-serif-mode #'mixed-pitch-mode) 1))
;;         (funcall #'mixed-pitch-mode (if +zen--original-mixed-pitch-mode-p 1 -1)))))
;;   (pushnew! writeroom--local-variables
;;             'display-line-numbers
;;             'visual-fill-column-width
;;             'org-adapt-indentation
;;             'org-superstar-headline-bullets-list
;;             'org-superstar-remove-leading-stars))


;; ;; Use pdf-tools to open PDF files
;; (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
;;       TeX-source-correlate-start-server t)

;; (setq +latex-viewers '(pdf-tools))

;; ;; Update PDF buffers after successful LaTeX runs
;; (add-hook 'TeX-after-compilation-finished-functions
;;           #'TeX-revert-document-buffer)

;; (after! lsp-pyright
;;   (lsp-register-client
;;    (make-lsp-client :new-connection (lsp-tramp-connection "pyright-langserver")
;;                     :major-modes '(python-mode)
;;                     :remote? t
;;                     :server-id 'pyright-remote)))


;; ;; (after! "lsp-javascript"
;; ;; (lsp-register-client
;; ;;     (make-lsp-client :new-connection (lsp-tramp-connection "typescript-language-server")
;; ;;                      :major-modes '(javascript-mode)
;; ;;                      :remote? t
;; ;;                      :server-id 'js-remote)))

;; (after! tramp
;;   (add-to-list 'tramp-remote-path "~/.local/bin"))





;; (use-package! nov
;;   :mode ("\\.epub\\'" . nov-mode)
;;   :config
;;   (map! :map nov-mode-map
;;         :n "RET" #'nov-scroll-up)

;;   (defun doom-modeline-segment--nov-info ()
;;     (concat
;;      " "
;;      (propertize
;;       (cdr (assoc 'creator nov-metadata))
;;       'face 'doom-modeline-project-parent-dir)
;;      " "
;;      (cdr (assoc 'title nov-metadata))
;;      " "
;;      (propertize
;;       (format "%d/%d"
;;               (1+ nov-documents-index)
;;               (length nov-documents))
;;       'face 'doom-modeline-info)))

;;   (advice-add 'nov-render-title :override #'ignore)

;;   (defun +nov-mode-setup ()
;;     (face-remap-add-relative 'variable-pitch
;;                              :family "Merriweather"
;;                              :height 1.1
;;                              :width 'semi-expanded)
;;     (face-remap-add-relative 'default :height 1.0)
;;     (setq-local line-spacing 0.2
;;                 next-screen-context-lines 4
;;                 shr-use-colors nil)
;;     (require 'visual-fill-column nil t)
;;     (setq-local visual-fill-column-center-text t
;;                 visual-fill-column-width 101
;;                 nov-text-width 100)
;;     (visual-fill-column-mode 1)
;;     (hl-line-mode -1)

;;     (add-to-list '+lookup-definition-functions #'+lookup/dictionary-definition)

;;     (setq-local mode-line-format
;;                 `((:eval
;;                    (doom-modeline-segment--workspace-name))
;;                   (:eval
;;                    (doom-modeline-segment--window-number))
;;                   (:eval
;;                    (doom-modeline-segment--nov-info))
;;                   ,(propertize
;;                     " %P "
;;                     'face 'doom-modeline-buffer-minor-mode)
;;                   ,(propertize
;;                     " "
;;                     'face (if (doom-modeline--active) 'mode-line 'mode-line-inactive)
;;                     'display `((space
;;                                 :align-to
;;                                 (- (+ right right-fringe right-margin)
;;                                    ,(* (let ((width (doom-modeline--font-width)))
;;                                          (or (and (= width 1) 1)
;;                                              (/ width (frame-char-width) 1.0)))
;;                                        (string-width
;;                                         (format-mode-line (cons "" '(:eval (doom-modeline-segment--major-mode))))))))))
;;                   (:eval (doom-modeline-segment--major-mode)))))

;;   (add-hook 'nov-mode-hook #'+nov-mode-setup))


(after! lsp-clangd
  (setq lsp-clients-clangd-args
        '("-j=3"
          "--background-index"
          "--clang-tidy"
          "--completion-style=detailed"
          "--header-insertion=never"
          "--header-insertion-decorators=0"))
  (set-lsp-priority! 'clangd 2))
